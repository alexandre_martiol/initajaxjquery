<?php

$lat = $_POST['lat'];
$lng = $_POST['lng'];
$dist = $_POST['dist'];

$con = mysqli_connect("localhost:3306", "root", "", "P5SGEMP") or die("Error loading database");
$query = mysqli_query($con, "select nom, x(arxiu.posicio) as lat, y(arxiu.posicio) as lng,(ST_Distance(arxiu.posicio,point(" . $lat . ", " . $lng . "))*111.195) as dist from arxiu where (ST_Distance(arxiu.posicio,point(" . $lat . ", " . $lng . "))*111.195)<" . $dist . ";");
$return = "";
$max = mysqli_num_rows($query);
$i = 0;

if (mysqli_num_rows($query) > 0) {
    $return = "[";
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        $return .= '{"nom":"' . $row['nom'] . '", "lat":"' . $row['lat'] . '", "lng":"' . $row['lng'] . '", "dist":"' . $row['dist'] . '"}';
        $i++;

        if($i < $max){
            $return.= ",";
        }
    }
    $return.= "]";
    echo $return;

} else {
    $return= '{"correct":"false"}';
    echo $return;
}
?>