create database if not exists P5SGEMP;
use P5SGEMP;

-- drop database P5SGEMP;

create table usuari (
    id serial,
    login_usuari varchar(20) unique,
    nom varchar(255),
    email varchar(255),
    contrasenya varchar(255),
    ultima_connexio date,
    pos_lat decimal,
    pos_long decimal
);

create table arxiu (
    id serial,
    id_propietari bigint unsigned,
    tipus int,
    nom varchar(20),
    descripcio varchar(255),
    path varchar(255),
	posicio point,
	constraint arxiu_usuari foreign key (id_propietari) references usuari(id)
);

create table descarrega (
    id_arxiu_descarregat bigint unsigned,
    id_usuari bigint unsigned,
    fecha date
);

insert into usuari (login_usuari, contrasenya, nom) values("cris", "1234", "Cristian");
insert into usuari (login_usuari, contrasenya, nom) values("alb", "1234", "Albert");
insert into usuari (login_usuari, contrasenya, nom) values("alex", "1234", "Alexandre");

insert into arxiu (id_propietari, nom, posicio) values(1,'Sagrada Familia', point(41.403490, 2.174166));
insert into arxiu (id_propietari, nom, posicio) values(2,'Pl. Catalunya', point(41.387016, 2.170043));
insert into arxiu (id_propietari, nom, posicio) values(1,'Pl. Espanya', point(41.375015, 2.149135));
insert into arxiu (id_propietari, nom, posicio) values(2,'Bar', point(41.429166, 2.174567));
insert into arxiu (id_propietari, nom, posicio) values(1,'Go3', point(41.355003, 2.108220));

select * from arxiu;
select id, nom, (ST_Distance(arxiu.posicio,point(41.385741, 2.165203))*111.195) as KM from arxiu; -- torna la distancia en KM
select nom, x(arxiu.posicio) as lat, y(arxiu.posicio) as lng,(ST_Distance(arxiu.posicio,point(41.385741, 2.165203))*111.195) as dist from arxiu where (ST_Distance(arxiu.posicio,point(41.385741, 2.165203))*111.195)<5;