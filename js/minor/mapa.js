
/*
 * Alexandre Martinez Olmos
 * 2n DAM
 * 9 - 3 - 2015
 */

function obreMapa(distancia) {
    $('#map').gmap3({
        map:{
            options:{
                draggable:false,
                scrollwheel: false,
                zoom: 15
            }
        },
        getgeoloc: {
            callback: function (latLng) {
                if (latLng) {
                    $(this).gmap3({
                        marker: {
                            latLng: latLng
                        },

                        map: {
                            options: {
                                center:latLng
                            }
                        }
                    });
                }
            }
        }
    });
}

