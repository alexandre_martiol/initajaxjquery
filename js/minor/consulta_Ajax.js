/* 
 * Alexandre Martinez Olmos
 * 2n DAM
 * 9 - 3 - 2015
 */

function enviaAjaxJSON() {
    var nom = $('#nom').val();
    var pass = $('#pass').val();
    $.ajax({
        type: "POST",
        url: "respostaAjaxJSON.php",
        dataType: "json",
        data: {nom: nom, pass: pass},
        success: function (respJSON) {
            if (respJSON.correct == "false") {
                document.getElementById("error").style.display = "inline-block";
                return false;
            }
            else {
                document.getElementById("error").style.display = "none";
                document.getElementById("content").style.display = "inline-block";
                obreMapa();
                var nom = respJSON.name;
                var pass = respJSON.pass;
            }
        }
    });
    return false;
}

function consultaPosicions() {
    var dist = $('#dist').val();
    var lat;
    var lng;
    var mapa = $("#map").gmap3({
        get: {
            name: 'map',
            callback: function (map) {
                center = map.getCenter();
                lat = center.lat();
                lng = center.lng();
            }
        }
    });

    $.ajax({
        type: "POST",
        url: "retornaPosicions.php",
        dataType: "json",
        data: {lat: lat, lng: lng, dist: dist},
        success: function (respJSON) {
            if (respJSON.correcto == "false") {
                alert("No s'han trobat posicions tan a prop");
            }
            else {
                $('#map').gmap3({
                    clear: {
                        name: ["marker"]
                    }
                });

                var length = respJSON.length;
                for (var i = 0; i < length; i++) {

                    $("#map").gmap3({
                        marker: {
                            values: [
                                {latLng: [respJSON[i].lat, respJSON[i].lng], data: respJSON[i].nom}
                            ],
                            options: {
                                draggable: false
                            }
                        }
                    });
                }
            }
            return false;
        }
    });
}
