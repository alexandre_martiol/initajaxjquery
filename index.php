<!--
    Alexandre Martinez Olmos
    2n DAM
    9 - 3 - 2015
-->

<html>
    <head>
        <link type="text/css" href="css/style.css" rel="stylesheet"/>
        <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="js/gmap3.min.js"></script>
        <script type="text/javascript" src="js/minor/consulta_Ajax.js"></script>
        <script type="text/javascript" src="js/minor/mapa.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
        <title></title>
    </head>
    <body>
        <div>
            <input type="text" name="nom" id="nom" placeholder="username"/>
            <input type="password" name="pass" id="pass" placeholder="password"/>
            <button onclick="enviaAjaxJSON();">Login</button>
            <br>

            <div id="error" style="display: none">
                Les dades introduides no son correctes.
            </div>
            <br>

            <div id="content" style="display: none">
                Distancia: <input type="text" id="dist"/><button onclick="consultaPosicions()">Actualitza</button>
                <br>
                <div id="map"></div>
            </div>
        </div>
    </body>
</html>


